from flask import Flask, request, jsonify
import psycopg2

app = Flask(__name__)

# Replace the database connection details with your own
DB_HOST = 'localhost'
DB_NAME = 'my_database'
DB_USER = 'admin'
DB_PASSWORD = 'admin'

def create_table():
    conn = psycopg2.connect(
        host=DB_HOST,
        dbname=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD
    )
    cur = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS data_table (
            id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            weight_value FLOAT,
            time DATE
        )
    """)
    conn.commit()
    conn.close()

@app.route('/insert', methods=['POST'])
def insert_data():
    data = request.get_json()

    if not data:
        return jsonify({'message': 'No JSON data provided'}), 400

    name = data.get('name')
    weight_value = data.get('weight_value')
    time = data.get('time')

    if not name or not weight_value or not time:
        return jsonify({'message': 'Missing required fields'}), 400

    try:
        conn = psycopg2.connect(
            host=DB_HOST,
            dbname=DB_NAME,
            user=DB_USER,
           password=DB_PASSWORD
        )
        cur = conn.cursor()

        cur.execute("""
            INSERT INTO data_table (name, weight_value, time)
            VALUES (%s, %s, %s)
        """, (name, weight_value, time))

        conn.commit()
        conn.close()

        return jsonify({'message': 'Data inserted successfully'}), 200
    except Exception as e:
        return jsonify({'message': str(e)}), 500

if __name__ == '__main__':
    create_table()
    app.run(host='0.0.0.0', port=5000)

