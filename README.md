# Terraform Project

This project aims to automate the infrastructure provisioning for a Flask application using Terraform and Docker Compose.

## Project Structure

The project is organized into three branches:
- `main`: Contains the main README.md file.
- `feature-terraform-infrastructure`: Contains the Docker Compose configuration and Flask app.
- `third-branch`: Contains the app.py file for the Flask application.




